package br.up.edu.oapp04;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * App 4. Desenvolva um aplicativo que leia a
     * temperatura em graus Celsius e a apresente
     * convertida em graus Fahrenheit. A fórmula
     * de conversão é: F = (9 * C + 160) / 5
     * */
    public void converter(View v){

        EditText cxCelsius = (EditText) findViewById(R.id.txtCelsius);
        String txtCelsius = cxCelsius.getText().toString();
        double vlrCelsius = Double.parseDouble(txtCelsius);
        double vlrFahrenheit = (9 * vlrCelsius + 160) / 5;
        EditText cxFahrenheit = (EditText) findViewById(R.id.txtFahrenheit);
        cxFahrenheit.setText(String.valueOf(vlrFahrenheit));

    }
}